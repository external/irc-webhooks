package main

import (
    "encoding/json"
    "io/ioutil"
    "os"
)

type Config struct {
    Secret      string   `json:"secret"`
    BindHost    string   `json:"bind_host"`
    BindPort    string   `json:"bind_port"`
    ServerHost  string   `json:"server_host"`
    ServerPort  string   `json:"server_port"`
    ServerSSL   bool     `json:"server_ssl"`
    ServerName  string   `json:"server_name"`
    Nick        string   `json:"nick"`
    Ident       string   `json:"ident"`
    Realname    string   `json:"realname"`
    NsNick      string   `json:"ns_nick"`
    NsPasswd    string   `json:"ns_passwd"`
    ChansToJoin []string `json:"chans_to_join"`
}

func getConfig() Config {
    f, err := os.Open("config.json")
    if err != nil {
        panic(err)
    }

    defer f.Close()
    out := Config{}
    data, err := ioutil.ReadAll(f)
    if err != nil {
        panic(err)
    }

    json.Unmarshal(data, &out)
    return out
}
