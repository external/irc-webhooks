package main

import (
    "crypto/tls"
    "fmt"
    "log"
    "net"
)

type Server struct {
    Host   string
    Port   string
    Name   string
    SSL    bool
    Conn   net.Conn
    Logger log.Logger
}

func NewServer(host, port, name string, ssl bool, logger log.Logger) *Server {
    return &Server{
        host,
        port,
        name,
        ssl,
        nil,
        logger,
    }
}

func (s *Server) Connect() error {
    var conn net.Conn
    var err error

    if s.SSL {
        conn, err = tls.Dial("tcp", fmt.Sprintf("%s:%s", s.Host, s.Port), nil)
    } else {
        conn, err = net.Dial("tcp", fmt.Sprintf("%s:%s", s.Host, s.Port))
    }
    if err != nil {
        return err
    }
    s.Conn = conn
    return nil
}
