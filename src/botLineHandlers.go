package main

import (
    "fmt"
    "github.com/goshuirc/irc-go/ircmsg"
)

func createPong(ping ircmsg.IrcMessage) ircmsg.IrcMessage {
    return ircmsg.MakeMessage(nil, "", "PONG", ping.Params[0])
}

func (b *Bot) HandleLine(lineIn string) {
    line, err := ircmsg.ParseLine(lineIn)
    if err != nil {
        b.Logger.Printf("could not parse IRC line: %v", err)
        return
    }

    b.Logger.Printf("<< %s", line.SourceLine)

    switch line.Command {
    case "PING":
        b.sendRaw(createPong(line))

    case "001":
        b.OnWelcome()
    }
}

func (b *Bot) OnWelcome() {
    if b.NSNick != "" && b.NSPasswd != "" {
        b.sendPrivMessage("NickServ", fmt.Sprintf("IDENTIFY %s %s", b.NSNick, b.NSPasswd))
    }

    for _, channel := range b.ChansToJoin {
        b.joinChannel(channel)
    }
}
