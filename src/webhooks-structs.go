package main

type RepoUser struct {
    ID        float64 `json:"id"`
    Login     string  `json:"login"`
    FullName  string  `json:"full_name"`
    Email     string  `json:"email"`
    AvatarUrl string  `json:"avatar_url"`
    Language  string  `json:"language"`
    Username  string  `json:"username"`
}

type Repository struct {
    ID              float64         `json:"id"`
    Owner           RepoUser        `json:"owner"`
    Name            string          `json:"name"`
    FullName        string          `json:"full_name"`
    Description     string          `json:"description"`
    Empty           bool            `json:"empty"`
    Private         bool            `json:"private"`
    Fork            bool            `json:"fork"`
    Parent          string          `json:"parent"`
    Mirror          bool            `json:"mirror"`
    Size            float64         `json:"size"`
    HtmlUrl         string          `json:"html_url"`
    SshUrl          string          `json:"ssh_url"`
    CloneUrl        string          `json:"clone_url"`
    Website         string          `json:"website"`
    StarsCount      float64         `json:"stars_count"`
    ForksCount      float64         `json:"forks_count"`
    WatchersCount   float64         `json:"watchers_count"`
    OpenIssuesCount float64         `json:"open_issues_count"`
    DefaultBranch   string          `json:"default_branch"`
    Archived        bool            `json:"archived"`
    CreatedAt       string          `json:"created_at"`
    UpdatedAt       string          `json:"updated_at"`
    Permissions     RepoPermissions `json:"permissions"`
}

type RepoPermissions struct {
    Admin bool
    Push  bool
    Pull  bool
}

type Commit struct {
    Hash         string      `json:"id"`
    Message      string      `json:"message"`
    Author       GitUser     `json:"author"`
    Committer    GitUser     `json:"committer"`
    Verification interface{} `json:"verification"`
    Timestamp    string      `json:"timestamp"`
}

type GitUser struct {
    Name     string `json:"name"`
    Email    string `json:"email"`
    Username string `json:"username"`
}

type Issue struct {
    Id          float64  `json:"id"`
    Url         string   `json:"url"`
    Number      float64  `json:"number"`
    User        RepoUser `json:"user"`
    Title       string   `json:"title"`
    Body        string   `json:"body"`
    Labels      []string `json:"labels"`
    Milestone   string   `json:"milestone"`
    Assignee    string   `json:"assignee"`
    Assignees   []string `json:"assignees"`
    State       string   `json:"state"`
    Comments    float64  `json:"comments"`
    CreatedAt   string   `json:"created_at"`
    UpdatedAt   string   `json:"updated_at"`
    ClosedAt    string   `json:"closed_at"`
    DueDate     string   `json:"due_date"`
    PullRequest string   `json:"pull_request"`
}

type IssueComment struct {
    Id             float64  `json:"id"`
    HtmlUrl        string   `json:"html_url"`
    PullRequestUrl string   `json:"pull_request_url"`
    IssueUrl       string   `json:"issue_url"`
    User           RepoUser `json:"user"`
    Body           string   `json:"body"`
    CreatedAt      string   `json:"created_at"`
    UpdatedAt      string   `json:"updated_at"`
}

type BaseWebHook struct {
    Secret string `json:"secret"`
}

type Push struct {
    BaseWebHook
    Ref        string     `json:"ref"`
    Before     string     `json:"before"`
    After      string     `json:"after"`
    CompareUrl string     `json:"compare_url"`
    Commits    []Commit   `json:"commits"`
    Repository Repository `json:"repository"`
    Pusher     RepoUser   `json:"pusher"`
    Sender     RepoUser   `json:"sender"`
}

type IssueAction struct {
    BaseWebHook
    Action     string       `json:"action"`
    Number     float64      `json:"number"`
    Issue      Issue        `json:"issue"`
    Repository Repository   `json:"repository"`
    Sender     RepoUser     `json:"sender"`
    Comment    IssueComment `json:"comment"`
}

type CreateAction struct {
    BaseWebHook
    Sha        string     `json:"sha"`
    Ref        string     `json:"ref"`
    RefType    string     `json:"ref_type"`
    Repository Repository `json:"repository"`
    Sender     RepoUser   `json:"sender"`
}
