package main

import (
    "bufio"
    "github.com/goshuirc/irc-go/ircmsg"
    "log"
    "os"
)

type Bot struct {
    Nick        string
    Ident       string
    Realname    string
    Server      Server
    ChansToJoin []string
    NSPasswd    string
    NSNick      string
    Logger      log.Logger
    MsgChan     chan string
}

type BotConfig struct {
    Nick        string
    Ident       string
    Realname    string
    ChansToJoin []string
    NSNick      string
    NSPasswd    string
    MsgChan     chan string
}

func newBot(config BotConfig, host, port, connectionName string, ssl bool) *Bot {
    logger := *log.New(os.Stdout, "", log.LstdFlags)

    return &Bot{
        Nick:        config.Nick,
        Ident:       config.Ident,
        Realname:    config.Realname,
        NSPasswd:    config.NSPasswd,
        NSNick:      config.NSNick,
        ChansToJoin: config.ChansToJoin,
        Server:      *NewServer(host, port, connectionName, ssl, logger),
        Logger:      logger,
        MsgChan:     config.MsgChan,
    }
}

func (b *Bot) run() {
    err := b.Server.Connect()
    if err != nil {
        b.Logger.Fatal(err)
    }

    go b.sendLoop()

    b.sendRaw(MakeMessage("NICK", b.Nick))
    b.sendRaw(MakeMessage("USER", b.Ident, "0", "0", b.Realname))
    b.readLoop()
}

func (b *Bot) send(message string) {
    b.MsgChan <- message
}

func (b *Bot) sendRaw(message ircmsg.IrcMessage) {
    line, err := message.LineBytes()
    if err != nil {
        b.Logger.Printf("could not convert IrcMessage to string: %v", err)
        return
    }
    b.Logger.Printf(">> %s", line)
    b.Server.Conn.Write(line)
}

func (b *Bot) readLoop() {
    scanner := bufio.NewScanner(b.Server.Conn)
    for scanner.Scan() {
        b.HandleLine(scanner.Text())
    }
}

func (b *Bot) joinChannel(channelName string) {
    b.sendRaw(MakeMessage("JOIN", channelName))
}

func (b *Bot) sendPrivMessage(target, message string) {
    b.sendRaw(MakeMessage("PRIVMSG", target, message))
}

func (b *Bot) fanOutMsg(message string) {
    for _, c := range b.ChansToJoin {
        b.sendPrivMessage(c, message)
    }
}

func (b *Bot) sendLoop() {
    for {
        select {
        case msg := <- b.MsgChan:
            b.fanOutMsg(msg)
        }
    }
}
