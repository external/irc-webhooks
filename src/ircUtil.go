package main

import "github.com/goshuirc/irc-go/ircmsg"

func MakeMessage(command string, params... string) ircmsg.IrcMessage{
    return ircmsg.MakeMessage(nil, "", command, params...)
}
