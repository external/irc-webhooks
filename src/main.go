package main

import "fmt"

const configPrint = `
CONFIG:
    Secret:   %q
    BindHost: %q
    BindPort: %q
`

func main() {
    config := getConfig()
    fmt.Printf(configPrint, config.Secret, config.BindHost, config.BindPort)
    msgChan := make(chan string)
    bc := BotConfig{
        Nick:  config.Nick,
        Ident: config.Ident,
        Realname: config.Realname,
        ChansToJoin: config.ChansToJoin,
        MsgChan: msgChan,
    }

    bot := newBot(bc, config.ServerHost, config.ServerPort, config.ServerName, config.ServerSSL)

    go bot.run()
    ListenForWebHook(config, msgChan)
}
